
resource "oci_identity_compartment" "compartment" {
  count = "${length(var.compartments)}"
  name           = "${element(var.compartments, count.index)}"
  description    = "compartment created by terraform for ADW Demo"
  compartment_id = "${var.tenancy_ocid}"
  enable_delete  = true       // true will cause this compartment to be deleted when running `terrafrom destroy`
}

data "oci_identity_compartments" "compartments" {
  compartment_id = "${oci_identity_compartment.compartment.*.compartment_id[count.index]}"

  filter {
    name   = "name"
    values = ["adw_demo"]
  }
}
/*
output "compartments" {
  value = "${oci_identity_compartment.compartment.*.name}"
}
*/
